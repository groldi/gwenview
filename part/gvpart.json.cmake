{
    "KPlugin": {
        "Icon": "gwenview",
        "InitialPreference": 12,
        "MimeTypes": [
            "${IMAGE_MIME_LIST}"
        ],
        "Name": "Gwenview Image Viewer",
        "Name[ca]": "Visualitzador d'imatges Gwenview",
        "Name[cs]": "Prohlížeč obrázků Gwenview",
        "Name[es]": "Visor de imágenes Gwenview",
        "Name[ka]": "Gwenview - სურათების დამთვალიერებელი",
        "Name[nl]": "GWenview Afbeeldingenviewer",
        "Name[pt]": "Visualizador de Imagens Gwenview",
        "Name[pt_BR]": "Visualizador de imagens do Gwenview",
        "Name[tr]": "Gwenview Görsel Görüntüleyici",
        "Name[uk]": "Переглядач зображень Gwenview",
        "Name[x-test]": "xxGwenview Image Viewerxx",
        "ServiceTypes": [
            "KParts/ReadOnlyPart"
        ]
    }
}
